# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-27 19:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('packages', '0003_package_city'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='property_type',
            field=models.CharField(choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)], default='HL', max_length=3),
        ),
    ]
