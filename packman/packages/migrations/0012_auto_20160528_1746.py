# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-28 12:16
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('packages', '0011_remove_booking_no_of_children'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='booking',
            name='guest',
        ),
        migrations.RemoveField(
            model_name='booking',
            name='length',
        ),
        migrations.RemoveField(
            model_name='booking',
            name='package',
        ),
        migrations.DeleteModel(
            name='Booking',
        ),
    ]
