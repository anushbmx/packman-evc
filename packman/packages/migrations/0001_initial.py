# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-27 19:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django_countries.fields
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('general', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=300, null=True)),
                ('address_1', models.CharField(max_length=300, null=True)),
                ('address_2', models.CharField(max_length=300, null=True)),
                ('state', models.CharField(max_length=150, null=True)),
                ('country', django_countries.fields.CountryField(max_length=2, null=True)),
                ('pincode', models.CharField(max_length=8, null=True)),
                ('phone', phonenumber_field.modelfields.PhoneNumberField(max_length=128, null=True)),
                ('property_type', models.CharField(default='HL', max_length=3)),
                ('base_occupancy', models.IntegerField(choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)], default=0)),
                ('extra_bed_allowed', models.BooleanField(default=False)),
                ('number_of_extra_bed', models.IntegerField(choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)], default=0)),
                ('base_price', models.DecimalField(decimal_places=2, default=0.0, max_digits=6)),
                ('extra_bed_price', models.DecimalField(decimal_places=2, default=0.0, max_digits=6)),
                ('featured', models.BooleanField(default=1)),
                ('description', models.CharField(max_length=600, null=True)),
                ('short_description', models.CharField(max_length=1000, null=True)),
                ('image_1', models.ImageField(blank=True, null=True, upload_to='records/')),
                ('image_2', models.ImageField(blank=True, null=True, upload_to='records/')),
                ('image_3', models.ImageField(blank=True, null=True, upload_to='records/')),
                ('image_4', models.ImageField(blank=True, null=True, upload_to='records/')),
                ('image_5', models.ImageField(blank=True, null=True, upload_to='records/')),
                ('image_6', models.ImageField(blank=True, null=True, upload_to='records/')),
                ('image_7', models.ImageField(blank=True, null=True, upload_to='records/')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('time', models.DateTimeField(auto_now=True)),
                ('status', models.BooleanField(default=1)),
                ('amenities', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='general.Amenity')),
                ('length', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='general.PackageLength')),
            ],
        ),
    ]
