from django.conf.urls import url
from django.views.generic import RedirectView

import views

urlpatterns = [
  # Property Management
  url(r'^$', views.package_list, name='package.index'),
  url(r'^$', views.package_list, name='package.list'),
  url(r'^(?P<package_id>[0-9]+)/$', views.package_info, name='package.info'),
]