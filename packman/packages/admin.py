from django.contrib import admin
from .models import Package
from django import forms
from django.forms import TextInput, Textarea

# Register your models here.

class PackagesForm(forms.ModelForm):
	description = forms.CharField( widget=forms.Textarea(attrs={'rows': 5, 'cols': 100}))
	class Meta:
		model = Package
		fields = ('__all__')

def disable_packages(modeladmin, request, queryset):
    queryset.update(status=False)
disable_packages.short_description = "Mark selected packges as disabled"

class PackageAdmin(admin.ModelAdmin):
	list_display = ['id', 'name', 'phone']
	actions = [disable_packages]
	form = PackagesForm

admin.site.register(Package,PackageAdmin)
