from __future__ import unicode_literals

from django.db import models
from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField
from general.models import *

# Create your models here.


class Package(models.Model):

  HOTEL = 'HL'
  VILLA = 'VA'
  COTTAGE = 'CE'
  PROPERTY_TYPE = (
    (HOTEL, 'Hotel'),
    (VILLA, 'Villa'),
    (COTTAGE, 'Cottage'),
  )

  CHOICES = [(i,i) for i in range(1,11)]
  
  name = models.CharField(max_length=300, null=False)
  address_1 = models.CharField(max_length=300, null=True, blank=True)
  address_2 = models.CharField(max_length=300, null=True, blank=True)
  city = models.ForeignKey(City, on_delete=models.PROTECT)
  state = models.CharField(max_length=150, null=True, blank=True)
  country = CountryField(null=True)
  pincode = models.CharField(max_length=8, null=True, blank=True)
  phone = PhoneNumberField(null=True)

  property_type_select =  models.ForeignKey(PropertyType, on_delete=models.PROTECT, default=1)
  base_occupancy = models.IntegerField(default=0, null=False, choices= [(i,i) for i in range(1,11)])
  number_of_extra_bed = models.IntegerField(default=0, null=False, choices= [(i,i) for i in range(0,11)])
  length = models.ManyToManyField(PackageLength)
  amenities = models.ManyToManyField(Amenity)

  # Price 
  base_price = models.DecimalField(max_digits=11, decimal_places=2, default=0.00)
  extra_bed_price = models.DecimalField(max_digits=11, decimal_places=2, default=0.00)

  # listing
  featured = models.BooleanField(default=0, null=False)
  description = models.CharField(max_length=1000, null=True, blank=True)
  short_description = models.CharField(max_length=1000, null=True, blank=True)
  host = models.ForeignKey(Host, on_delete=models.PROTECT)

  #Image
  image_1 = models.ImageField(upload_to='records/')
  image_2 = models.ImageField(upload_to='records/', null=True, blank=True)
  image_3 = models.ImageField(upload_to='records/', null=True, blank=True)
  image_4 = models.ImageField(upload_to='records/', null=True, blank=True)
  image_5 = models.ImageField(upload_to='records/', null=True, blank=True)
  image_6 = models.ImageField(upload_to='records/', null=True, blank=True)
  image_7 = models.ImageField(upload_to='records/', null=True, blank=True)

  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)
  
  def __str__(self):
    return '%s : %s' % (self.id, self.name)
