import datetime
from django import forms
from .models import Booking, Order, OrderItems

class booking_add_form(forms.ModelForm):
  campagin_start = datetime.datetime.strptime('12-17-2016', "%m-%d-%Y").date()
  campagin_end = datetime.datetime.strptime('12-18-2016', "%m-%d-%Y").date()

  if datetime.date.today() > campagin_start  :
    start_date = datetime.date.today()
  else :
    start_date = campagin_start

  delta =  campagin_end -  campagin_start
  campagin_days = int(delta.days)

  DATECHOICE=[(campagin_start + datetime.timedelta(hours=date*24), start_date + datetime.timedelta(hours=date*24)) for date in range(0,campagin_days)]
  checkin = forms.ChoiceField(choices=DATECHOICE)


  def __init__(self, *args, **kwargs):
    package = kwargs.pop('package', None)
    super(booking_add_form, self).__init__(*args, **kwargs)
    if package is not None:
      no_of_guest_query = package.length.all()
      self.fields['length']  =  forms.ModelChoiceField(label="Package",queryset=no_of_guest_query, initial=1, widget=forms.RadioSelect(), empty_label=None)
      self.fields['no_of_guest'] = forms.ChoiceField(choices= [(i,i) for i in range(1,package.base_occupancy+1)], initial=(package.base_occupancy))
      self.fields['no_of_extra_bed'] = forms.ChoiceField(label="Extra *", choices=[(0, 'None')] + [(i,i) for i in range(1,package.number_of_extra_bed+1)], required=False)

  class Meta:
    model = Booking
    fields = ('checkin','length', 'no_of_guest', 'no_of_extra_bed')

class order_item_add_form(forms.ModelForm):
  CHOICES = [(i,i) for i in range(1, 10)]
  quantity = forms.ChoiceField(choices=CHOICES)
  addon_form = forms.IntegerField(widget = forms.HiddenInput(), required=False, initial=1)

  def __init__(self, *args, **kwargs):
    super(order_item_add_form, self).__init__(*args, **kwargs)
    self.fields['add_on'].label = "Avalible Add-on"
    self.fields['add_on'].empty_label = None
    
  class Meta:
    model = OrderItems
    fields =('add_on','quantity')
    widgets = {
        'add_on': forms.RadioSelect(),
    }

class order_promo_add_form(forms.Form):
  promo_code = forms.CharField(required=True, label="Promo Code", max_length="20", widget=forms.TextInput(attrs={'class':'input-group-field'}))
  promo_form = forms.IntegerField(widget = forms.HiddenInput(), required=False, initial=1)

class order_confirm_form(forms.Form):
  OPTIONS = (
    (0, 'Full Payment'),
    (1, 'Part Payment'),
  )
  payment_type = forms.ChoiceField(choices=OPTIONS, widget=forms.RadioSelect(), required=True)
  order_form = forms.IntegerField(widget = forms.HiddenInput(), required=False, initial=1)