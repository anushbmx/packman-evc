from django.conf.urls import url
from django.views.generic import RedirectView

import views

urlpatterns = [
  # Property Management
  # url(r'^$', views.booking_index, name='package.index'),  
  url(r'^$', views.booking_list, name='booking.list'),
  url(r'^auth/$', views.booking_auth, name='booking.auth'),
  url(r'^payment/success/$', views.booking_payment_success, name='booking.payment.success'),
  url(r'^payment/error/$', views.booking_payment_error, name='booking.payment.error'),
  url(r'^payment/cancel/$', views.booking_payment_cancel, name='booking.payment.cancel'),
  url(r'^invoice/(?P<payment_hash>[0-9a-zA-Z]+)/$', views.booking_payment, name='booking.payment'),
  url(r'^(?P<booking_id>[a-zA-Z0-9]+)/$', views.booking_info, name='booking.info'),
  url(r'^(?P<booking_id>[a-zA-Z0-9]+)/review/$', views.booking_review, name='booking.review'),
  url(r'^(?P<booking_id>[a-zA-Z0-9]+)/review/(?P<order_id>[0-9]+)/(?P<order_item_id>[0-9]+)/remove_item$', views.booking_order_item_remove, name='booking.order.item.remove'),
  url(r'^(?P<booking_id>[a-zA-Z0-9]+)/review/(?P<order_id>[0-9]+)/(?P<promo_id>[0-9]+)/remove_promocode$', views.booking_promo_code_remove, name='booking.order.promo_code.remove'),
]