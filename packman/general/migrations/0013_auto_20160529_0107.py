# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-28 19:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0012_addon_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addon',
            name='quantity',
            field=models.IntegerField(default=-1, help_text='-1 For unlimited quantity'),
        ),
    ]
