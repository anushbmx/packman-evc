from django.contrib import admin
from .models import *
# Register your models here.


class AddonAdmin(admin.ModelAdmin):
	list_display = ['id', 'name', 'price']

class PromoAdmin(admin.ModelAdmin):
	list_display = ['id', 'promo_code', 'price']

class PackageLengthAdmin(admin.ModelAdmin):
	list_display = ['id', 'nights']

class AmenityAdmin(admin.ModelAdmin):
	list_display = ['id', 'name']

class CityAdmin(admin.ModelAdmin):
	list_display = ['id', 'name']

class HostAdmin(admin.ModelAdmin):
	list_display = ['id', 'name', 'email', 'phone']

class GuestAdmin(admin.ModelAdmin):
	list_display = ['id', 'name', 'email', 'phone']

class PropertyTypeAdmin(admin.ModelAdmin):
	list_display = ['id', 'name']

admin.site.register(PropertyType,PropertyTypeAdmin)
admin.site.register(Addon,AddonAdmin)
admin.site.register(Promo,PromoAdmin)
admin.site.register(PackageLength,PackageLengthAdmin)
admin.site.register(Amenity,AmenityAdmin)
admin.site.register(City,CityAdmin)
admin.site.register(Host,HostAdmin)
admin.site.register(Guest,GuestAdmin)