from django.conf.urls import url

from public import views


urlpatterns = [
    url(r'^$', views.home, name='public.home'),
    url(r'^faq/$', views.faq, name='public.faq'),
    url(r'^terms/$', views.terms, name='public.terms'),
]