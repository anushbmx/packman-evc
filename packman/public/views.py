from django.shortcuts import render
from packages.models import Package
from .models import Faq, Terms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.

def home(request):
  data = {}
  packages = Package.objects.filter(status=1).order_by('-featured', 'id')
  paginator = Paginator(packages, 8) # Show 25 contacts per page

  page = request.GET.get('page')
  try:
      packages = paginator.page(page)
  except PageNotAnInteger:
  # If page is not an integer, deliver first page.
      packages = paginator.page(1)
  except EmptyPage:
  # If page is out of range (e.g. 9999), deliver last page of results.
      packages = paginator.page(paginator.num_pages)

  data['packages'] = packages
  data['navigation'] = False
  data['title'] = 'Enchanted Valley Carnival 2016' 
  return render(request,'public/index.html', data)

def faq(request):
  data = {}
  faqs = Faq.objects.filter(status=1)
  data['faqs'] = faqs
  data['title'] = 'Frequently Asked Questions (FAQs)'
  return render(request,'public/faq.html', data)

def terms(request):
  data = {}
  terms = Terms.objects.filter(status=1)
  data['terms'] = terms
  data['title'] = 'Terms and Conditions'
  return render(request,'public/terms.html', data)
