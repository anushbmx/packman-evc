from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Faq(models.Model):
  question = models.CharField(max_length =400)
  answer = models.CharField(max_length =15000)
  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

  def __str__(self):
    return '%s : %s' % (self.id, self.question)

class Terms(models.Model):
  term = models.CharField(max_length =15000)
  # Status
  created = models.DateTimeField(auto_now_add=True,  editable=False)
  time = models.DateTimeField(auto_now=True,  editable=False)
  status = models.BooleanField(default=1, null=False)

  def __str__(self):
    return '%s : %s' % (self.id, self.term)