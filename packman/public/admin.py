from django.contrib import admin
from .models import Faq, Terms
from django import forms
from django.forms import TextInput, Textarea

# Register your models here.

class FaqForm(forms.ModelForm):
	answer = forms.CharField( widget=forms.Textarea(attrs={'rows': 5, 'cols': 100}))
	class Meta:
		model = Faq
		fields = ('__all__')

class FaqsAdmin(admin.ModelAdmin):
	list_display = ['id', 'question', 'status']
	form = FaqForm

class TermsForm(forms.ModelForm):
	term = forms.CharField( widget=forms.Textarea(attrs={'rows': 5, 'cols': 100}))
	class Meta:
		model = Terms
		fields = ('__all__')

class TermsAdmin(admin.ModelAdmin):
	list_display = ['id', 'term']
	form = TermsForm

admin.site.register(Faq,FaqsAdmin)
admin.site.register(Terms,TermsAdmin)