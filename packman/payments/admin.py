from django.contrib import admin
from .models import *
# Register your models here.

class PaymentsAdmin(admin.ModelAdmin):
	list_display = ['id', 'hash', 'booking', 'order', 'total', 'payment_status']

admin.site.register(Payments,PaymentsAdmin)